<div>
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($statusUpdate)
        <livewire:contact-update></livewire:contact-update>
    @else
        <livewire:contact-create></livewire:contact-create>
    @endif

    <hr>

    <div class="row">
        <div class="col">
            <select wire:model="paginate" name="" id="" class="form-control w-auto">
                <option value="5">5</option>
                <option value="10">10</option>
            </select>
        </div>
        <div class="col">
            <input wire:model="search"type="text" class="form-control" placeholder="Search">
        </div>

    </div>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>NO</th>
                <th>Name</th>
                <th>Phone</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($contacts as $contact)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>
                        <button wire:click="getContact({{ $contact->id }})"
                            class="btn btn-sm btn-info text-white">Edit</button>
                        <button wire:click="destroy({{ $contact->id }})"
                            class="btn btn-sm btn-danger text-white">Delete</button>
                    </td>
                </tr>
            @empty
                <p>tidak ada</p>
            @endforelse
        </tbody>
    </table>
    <div class="w-100">
        {{ $contacts->links() }}
    </div>
</div>
