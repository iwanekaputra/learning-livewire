<div>
    <form wire:submit.prevent="update">
        <div class="form-group">
            <div class="form-row">
                <div class="col">
                    <input type="hidden" wire:model="contactId">
                    <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror"
                        placeholder="Name">
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="col">
                    <input wire:model="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                        placeholder="Phone">
                    @error('phone')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-primary">SUbmit</button>

    </form>
</div>
